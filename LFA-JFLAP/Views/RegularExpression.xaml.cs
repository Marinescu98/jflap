﻿using LFA_JFLAP.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LFA_JFLAP.Views
{
    /// <summary>
    /// Interaction logic for RegularExpression.xaml
    /// </summary>
    public partial class RegularExpression : UserControl
    {
        private readonly RegularExpresionViewModel regularExpresionViewModel;
        private const string SUCCESS = "MATCH!";
        private const string FAIL = "NOT MATCH!";
        public RegularExpression()
        {
            InitializeComponent();
            regularExpresionViewModel = new RegularExpresionViewModel();
            regularExpresionViewModel.GetQuickReferences(QuickReferences);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(TestStringTb.Text!="" && RegularExpressionTb.Text != "")
            {
                if(!Regex.IsMatch(TestStringTb.Text,RegularExpressionTb.Text))
                {
                    MatchingLb.Content = FAIL;
                }
                else
                {
                    MatchingLb.Content = SUCCESS;
                }
            }
        }
    }
}
