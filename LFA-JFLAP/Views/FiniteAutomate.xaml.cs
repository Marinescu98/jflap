﻿using LFA_JFLAP.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LFA_JFLAP.Views
{
    /// <summary>
    /// Interaction logic for FiniteAutomate.xaml
    /// </summary>
    public partial class FiniteAutomate : UserControl
    {
        private FiniteAutomateViewModel faViewModel;
        public FiniteAutomate()
        {
            InitializeComponent();

            faViewModel = new FiniteAutomateViewModel();

            faViewModel.ButtonsEnabled(RedoFA, UndoFA);

            faViewModel.SaveButtonState(Save);
        }

        private void Canvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (StateCreatorFA.IsChecked == true)
            {
                faViewModel.CreateEllipse(CanvasFA, e);
                faViewModel.ButtonsEnabled(RedoFA, UndoFA);
            }

            if (TransitionCreatorFA.IsChecked == true)
            {
                faViewModel.CreateLine(CanvasFA, sender, e);
            }

            if (DeleteFA.IsChecked == true)
            {
                faViewModel.Delete(CanvasFA, sender, e);
            }

            Old.IsEnabled = false;
        }

        private void UndoFA_Click(object sender, RoutedEventArgs e)
        {
            faViewModel.Undo(CanvasFA);
            faViewModel.ButtonsEnabled(RedoFA, UndoFA);
            UndoFA.IsChecked = false;
            faViewModel.SaveButtonState(Save);
            faViewModel.RestoreButton(Old);
            
        }

        private void RedoFA_Click(object sender, RoutedEventArgs e)
        {
            faViewModel.Redo(CanvasFA);
            faViewModel.ButtonsEnabled(RedoFA, UndoFA);
            RedoFA.IsChecked = false;
            faViewModel.SaveButtonState(Save);

        }

        private void StateCreatorFA_Click(object sender, RoutedEventArgs e)
        {
            if (StateCreatorFA.IsChecked == true)
            {
                DeleteFA.IsEnabled = false;
                Save.IsEnabled = false;
                TransitionCreatorFA.IsEnabled = false;
            }
            else
            {
                DeleteFA.IsEnabled = true;
                Save.IsEnabled = true;
                TransitionCreatorFA.IsEnabled = true;
            }
            Old.IsEnabled = false;
        }

        private void DeleteFA_Click(object sender, RoutedEventArgs e)
        {
            if (DeleteFA.IsChecked == true)
            {
                StateCreatorFA.IsEnabled = false;
                TransitionCreatorFA.IsEnabled = false;
                Save.IsEnabled = false;
            }
            else
            {
                StateCreatorFA.IsEnabled = true;
                Save.IsEnabled = true;
                TransitionCreatorFA.IsEnabled = true;
            }
            Old.IsEnabled = false;
        }

        private void SaveFA_Click(object sender, RoutedEventArgs e)
        {
            if (faViewModel.Save() == true)
            {
                Save.IsChecked = false;
                Save.IsEnabled = true;
            }
            faViewModel.SaveButtonState(Save);
            Old.IsEnabled = false;
        }

        private void Old_Click(object sender, RoutedEventArgs e)
        {
            faViewModel.Restore(CanvasFA);
            Old.IsEnabled = false;
        }

        private void TransitionCreatorFA_Click(object sender, RoutedEventArgs e)
        {
            if (TransitionCreatorFA.IsChecked == true)
            {
                StateCreatorFA.IsEnabled = false;
                DeleteFA.IsEnabled = false;
                Save.IsEnabled = false;
            }
            else
            {
                StateCreatorFA.IsEnabled = true;
                DeleteFA.IsEnabled = true;
                Save.IsEnabled = true;
            }
            Old.IsEnabled = false;
        }
    }
}
