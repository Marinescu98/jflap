﻿
using LFA_JFLAP.Models;
using LFA_JFLAP.Utility;
using Microsoft.Data.SqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.RightsManagement;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace LFA_JFLAP.ViewModels
{
    public class FiniteAutomateViewModel
    {
        private static readonly SqlConnection connection = new SqlConnection(@"Data Source=DESKTOP-84N5291\SQLEXPRESS;Database=JFLAP;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        private static readonly List<Component> elements = new List<Component>();
        private static readonly Stack<Component> elementsRedo = new Stack<Component>();
        private static int _numberOfCircles = 0;
        private double flag = 0;
        private double X_line = 0, Y_line = 0;

        public FiniteAutomateViewModel()
        {

        }
        private double Distance(double x1, double x2, double y1, double y2)
        {
            return Math.Sqrt(Math.Pow((x1 - x2), 2) + Math.Pow((y1 - y2), 2));
        }
        private Component FindState(double X, double Y)
        {
            Component closer = null;
            foreach (var element in elements)
            {
                if (element.type == "ELLIPSE")
                {
                    if (closer == null)
                    {
                        closer = element;
                    }
                    else
                    {
                        if (Distance(X, element.x1, Y, element.y1) < Distance(X, closer.x1, Y, closer.y1))
                        {
                            closer = element;
                        }
                    }
                }
            }

            return closer;
        }
        public void ButtonsEnabled(ToggleButton redoBtn, ToggleButton undoBtn)
        {
            if (elementsRedo.Count == 0)
            {
                redoBtn.IsEnabled = false;
            }
            else
            {
                redoBtn.IsEnabled = true;
            }
            if (elements.Count == 0)
            {
                undoBtn.IsEnabled = false;
            }
            else
            {
                undoBtn.IsEnabled = true;
            }
        }
        public void Undo(Canvas canvas)
        {
            Component undo = elements[elements.Count - 1];
            if (undo.type == "ELLIPSE")
            {
                State state = (State)undo;
                canvas.Children.Remove(state.ellipse);
                canvas.Children.Remove(state.label);
                elements.Remove(state);
                elementsRedo.Push(state);
            }
            else
            {
                canvas.Children.Remove(undo.GetShape());
                elements.Remove(undo);
                elementsRedo.Push(undo);
            }

        }
        public void Redo(Canvas canvas)
        {
            Component redo = elementsRedo.Pop();

            if (redo.type == "ELLIPSE")
            {
                Canvas.SetLeft(redo.GetShape(), redo.x1);
                Canvas.SetTop(redo.GetShape(), redo.y1);
                canvas.Children.Add(redo.GetShape());

                Label label = new Label()
                {
                    Content = redo.tag,
                    FontSize = 20,

                };

                Canvas.SetLeft(label, redo.x2);
                Canvas.SetTop(label, redo.y2);
                canvas.Children.Add(label);

                elements.Add(redo);
            }
            else
            {
                if (redo.type == "LINE")
                {
                    Canvas.SetLeft(redo.GetShape(), redo.x2);
                    Canvas.SetTop(redo.GetShape(), redo.y2);
                    Canvas.SetLeft(redo.GetShape(), redo.x1);
                    Canvas.SetTop(redo.GetShape(), redo.y1);
                    canvas.Children.Add(redo.GetShape());

                    elements.Add(redo);
                }
            }



        }
        public void CreateEllipse(Canvas canvas, MouseButtonEventArgs e)
        {
            Ellipse ellipse = new Ellipse()
            {
                Height = 50,
                Width = 50,
                Stroke = Brushes.Black,
                Fill = Brushes.DodgerBlue,
                Tag = "s" + _numberOfCircles + 1
            };
            _numberOfCircles++;

            double X = e.GetPosition(canvas).X - 20;
            double Y = e.GetPosition(canvas).Y - 20;

            Label label = new Label()
            {
                Content = "s" + _numberOfCircles,
                FontSize = 20,

            };

            Canvas.SetLeft(ellipse, X);
            Canvas.SetTop(ellipse, Y);
            canvas.Children.Add(ellipse);

            Canvas.SetLeft(label, X + 4);
            Canvas.SetTop(label, Y + 3);
            canvas.Children.Add(label);

            elements.Add(new State
            {
                ellipse = ellipse,
                x1 = X,
                y1 = Y,
                label = label,
                x2 = X + 4,
                y2 = Y + 4,
                tag= "s" + _numberOfCircles
            });
        }
        public void Delete(Canvas canvas, object sender, MouseButtonEventArgs e)
        {
            Point point = e.GetPosition((Canvas)sender);
            HitTestResult result = VisualTreeHelper.HitTest(canvas, point);
            Line line = result.VisualHit as Line;

            if (line != null)
            {
                canvas.Children.Remove(line);
            }
            else
            {
                Canvas canvas1 = result.VisualHit as Canvas;
                if (canvas1 != null)
                {
                    return;
                }

                double X_temp = e.GetPosition(canvas).X - 20;
                double Y_temp = e.GetPosition(canvas).Y - 20;

                State ellipseLabel = (State)FindState(X_temp, Y_temp);

                if (ellipseLabel != null)
                {
                    canvas.Children.Remove(ellipseLabel.ellipse);
                    canvas.Children.Remove(ellipseLabel.label);
                    elements.Remove(ellipseLabel);
                    elementsRedo.Push(ellipseLabel);
                }
            }
        }
        public void RestoreButton(ToggleButton button)
        {
            if (_numberOfCircles == 0)
                button.IsEnabled = true;
            else
                button.IsEnabled = false;
        }
        public bool Save()
        {
            try
            {
                connection.Open();

                SqlCommand comm1 = new SqlCommand("DELETE FROM TState ", connection);
                comm1.ExecuteNonQuery();


                SqlCommand comm2 = new SqlCommand("DELETE FROM TLine ", connection);
                comm2.ExecuteNonQuery();

                foreach (var element in elements)
                {
                    if (element.type == "LINE")
                    {
                        Use.SaveComponent("INSERT INTO " +
                    "TLine(x1,y1,tag,x2,y2) " +
                    "VALUES(" + element.x1 + "," + element.y1 + ",'" + element.tag + "'," + element.x2 + "," + element.y2 + ")");
                    }
                    else
                    {
                        Use.SaveComponent("INSERT INTO " +
                    "TState(x_ellipse,y_ellipse,tag,x_label,y_label) " +
                    "VALUES(" + element.x1 + "," + element.y1 + ",'" + element.tag + "'," + element.x2 + "," + element.y2 + ")");
                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
                return false;
            }
            finally
            {
                connection.Close();
            }
            return true;
        }
        public void Restore(Canvas canvas)
        {
            Use.RestoreState(canvas,elements);
            Use.RestoreTransition(canvas,elements);
        }
        public void SaveButtonState(ToggleButton button)
        {
            button.IsEnabled = elements.Count == 0 ? false : true;
        }
        public void CreateLine(Canvas canvas, object sender, MouseButtonEventArgs e)
        {
            Point point = e.GetPosition((Canvas)sender);
            HitTestResult result = VisualTreeHelper.HitTest(canvas, point);
            Canvas canvas2 = result.VisualHit as Canvas;

            if (canvas2 != null)
            {
                return;
            }

            double X_temp = e.GetPosition(canvas).X;
            double Y_temp = e.GetPosition(canvas).Y;

            Component ellipseLabel = FindState(X_temp, Y_temp);

            if (flag == 0)
            {
                X_line = X_temp;
                Y_line = Y_temp;
                flag++;
            }
            else
            {
                Line line = new Line()
                {
                    Stroke = System.Windows.Media.Brushes.LightSteelBlue,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Center,
                    StrokeThickness = 4,
                    X1 = X_line,
                    X2 = X_temp,
                    Y1 = Y_line,
                    Y2 = Y_temp
            };

                flag--;
                canvas.Children.Add(line);

                elements.Add(new Transition
                {
                    line = line,
                    tag = ellipseLabel.tag,
                    x1 = X_line,
                    x2 = X_temp,
                    y1 = Y_line,
                    y2 = Y_temp
                });
            }

        }



    }
}
