﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Shapes;

namespace LFA_JFLAP.Models
{
    public abstract class Component
    {
        public string type;
        public double x1;
        public double x2;
        public double y1;
        public double y2;
        public string tag;
        public abstract Shape GetShape();
        public abstract void SetShape(Shape shape);
    }
}
