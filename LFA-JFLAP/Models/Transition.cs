﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Shapes;

namespace LFA_JFLAP.Models
{
    public class Transition:Component
    {
        public Line line;
        public Transition()
        {
            type = "LINE";
        }

        public override Shape GetShape()
        {
            return line;
        }

        public override void SetShape(Shape shape)
        {
            line = (Line)shape;
        }
    }
}
