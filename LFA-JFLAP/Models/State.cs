﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Text;
using System.Windows.Shapes;

namespace LFA_JFLAP.Models
{
    public class State : Component
    {

        public Label label { get; set; }       
        public Ellipse ellipse { get; set; }
        public State()
        {
            type = "ELLIPSE";
        }
        public override Shape GetShape()
        {
            return ellipse;  
        }
        public override void SetShape(Shape shape)
        {
            ellipse = (Ellipse)shape;
        }
    }
}
