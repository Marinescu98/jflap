﻿using LFA_JFLAP.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace LFA_JFLAP.Utility
{
    public static class Use
    {
        private static readonly SqlConnection connection = new SqlConnection(@"Data Source=DESKTOP-84N5291\SQLEXPRESS;Database=JFLAP;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

        public static bool SaveComponent(string sql)
        {
            try
            {
                connection.Open();
                SqlCommand comm = new SqlCommand(sql, connection);
                if (comm.ExecuteNonQuery() == 0)
                    return false;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
                return false;
            }
            finally
            {
                connection.Close();
            }
            return true;
        }
        public static bool RestoreState(Canvas canvas, List<Component> components)
        {
            string sql = "Select * from TState";
            int numberOfCircles = 0;
            try
            {
                connection.Open();
                using (SqlCommand comm = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = comm.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            Ellipse ellipse = new Ellipse()
                            {
                                Height = 50,
                                Width = 50,
                                Stroke = Brushes.Black,
                                Fill = Brushes.DodgerBlue,
                                Tag = reader["tag"].ToString()
                            };
                            numberOfCircles++;

                            Label label = new Label()
                            {
                                Content = reader["tag"].ToString(),
                                FontSize = 20
                            };

                            Canvas.SetLeft(ellipse, Convert.ToDouble(reader["x_label"].ToString()));
                            Canvas.SetTop(ellipse, Convert.ToDouble(reader["y_label"].ToString()));
                            canvas.Children.Add(ellipse);

                            Canvas.SetLeft(label, Convert.ToDouble(reader["x_label"].ToString()));
                            Canvas.SetTop(label, Convert.ToDouble(reader["y_label"].ToString()));
                            canvas.Children.Add(label);

                            components.Add(new State
                            {
                                ellipse = ellipse,
                                label = label,
                                tag = reader["tag"].ToString(),
                                x1 = Convert.ToDouble(reader["x_label"].ToString()),
                                x2 = Convert.ToDouble(reader["x_label"].ToString()),
                                y1 = Convert.ToDouble(reader["y_label"].ToString()),
                                y2 = Convert.ToDouble(reader["y_label"].ToString())
                            });
                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
                return false;
            }
            finally
            {
                connection.Close();
            }
            return true;
        }

        public static bool RestoreTransition(Canvas canvas, List<Component> components)
        {
            string sql = "Select * from TLine";

            try
            {
                connection.Open();
                using (SqlCommand comm = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = comm.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            Line lineL = new Line()
                            {
                                Stroke = System.Windows.Media.Brushes.LightSteelBlue,
                                HorizontalAlignment = HorizontalAlignment.Left,
                                VerticalAlignment = VerticalAlignment.Center,
                                StrokeThickness = 2,
                                X1 = Convert.ToDouble(reader["x1"].ToString()),
                                X2 = Convert.ToDouble(reader["x2"].ToString()),
                                Y1 = Convert.ToDouble(reader["y1"].ToString()),
                                Y2 = Convert.ToDouble(reader["y2"].ToString())
                            };
                            canvas.Children.Add(lineL);

                            components.Add(new Transition
                            {
                                line = lineL,
                                x1 = Convert.ToDouble(reader["x1"].ToString()),
                                x2 = Convert.ToDouble(reader["x2"].ToString()),
                                y1 = Convert.ToDouble(reader["y1"].ToString()),
                                y2 = Convert.ToDouble(reader["y2"].ToString())
                            });
                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
                return false;
            }
            finally
            {
                connection.Close();
            }
            return true;
        }
    }
}
