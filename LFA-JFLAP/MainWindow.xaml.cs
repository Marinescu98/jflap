﻿using LFA_JFLAP.ViewModels;
using System;
using System.Windows;


namespace LFA_JFLAP
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = new HomeViewModel();
        }

        private void Button_Click_Main(object sender, RoutedEventArgs e)
        {
            DataContext = new HomeViewModel();
        }
        private void Button_Click_FA(object sender, RoutedEventArgs e)
        {
            DataContext = new FiniteAutomateViewModel();
        }
        private void Button_Click_RE(object sender, RoutedEventArgs e)
        {
            DataContext = new RegularExpresionViewModel();
        }
    }
}
